<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'content', 'date','topic_id','user_id','image'
    ];

    public function getRules($option='add'){
        return [
            'content'=>'required|string',
            'topic_id'=>'required|string',
            'user_id'=>'required|exists:users,id'
        ];
    }
}
