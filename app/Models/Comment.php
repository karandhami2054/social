<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
        'post_id', 'user_id','comment'
    ];

    public function getRules($option='add'){
        return [
            'post_id'=>'required|exists:posts,id',
            'comment'=>'required|string',
            'user_id'=>'required|exists:users,id'
        ];
    }
}
