<?php
use app\User;

function uploadImage($image, $dir_name){
   $path = public_path().'/images/'.$dir_name;

   if(!File::exists($path)){
       File::makeDirectory($path,0777,true,true);
   }
   $file_name= ucfirst($dir_name).'-'.date('YmdHis').rand(0,999).'.'.$image->getClientOriginalExtension();
   $success= $image->move($path, $file_name);
   if($success){
       return $file_name;
   }else{
       return false;
   }
}

function username($id){
    $user = new User();
    $user_list = $user->find($id);
        echo $user_list['name'];
}


function useralldetails($id){
    $user = new User();
    $user_list = $user->find($id);
        echo '<h2>'.$user_list['name'].'</h2>';
        echo '<h4>'.$user_list['address'].'</h4>';
}