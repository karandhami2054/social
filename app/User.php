<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','age','contact','address','account','status','role','gender','image'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function getRules($option='add'){
        return [
            'name'=>'required|string',
            'email'=>(($option=='add')? 'required' : 'nullable').'|email',
            'age'=>'required|string',
            'address'=>'required|string',
            'contact'=>'required|string',
            'gender'=>'required|in:male,female,other',
            'role'=>'required|in:admin,user',
            'account'=>'required|in:activate,deactivate',
            'status'=>'required|in:active,inactive',
            'password'=>(($option=='add')? 'required' : 'nullable').'|string|confirmed',
        ];
    }



    public function user_info(){
        return $this->hasOne('App\User','id','user_id');
    }

    public function getUserDetails($id){
     return $this->with('user_info')->where('id',$id)->get();    
    }
    
}
