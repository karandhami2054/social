<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    protected $user=null;
    public function __construct(User $user)
    {
        $this->user=$user;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=$this->user->where('id','!=',Request()->user()->id)->get();
        return view('admin.user.index')->with('userdata',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.user.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $rules=$this->user->getRules();
        $request->validate($rules);
        //dd($request);
        $data=$request->all();
        $data['password']=Hash::make($data['password']);

        $this->user=$this->user->fill($data);
        $this->user->save();

        if($this->user){
            request()->session()->flash('success','User added successfully');
        }
        else{
            request()->session()->flash('error','User not added at this time. Please try again');
        }

        return redirect()->route('user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->user=$this->user->find($id);
        if(!$this->user){
            request()->session()->flash('error','banner does not found');
            return redirect()->route('user.index');
        }
        return view('admin.user.form')->with('user_data',$this->user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules=$this->user->getRules('update');
        $request->validate($rules);
        //dd($request);
        $data=$request->all();

        if($request->change_password){
            $data['password']= Hash::make($data['password']);
        }else{
            unset($data['password']);
        }
        $this->user= $this->user->find($id);
        $this->user=$this->user->fill($data);
        $this->user->save();

        if($this->user){
            request()->session()->flash('success','User updated successfully');
        }
        else{
            request()->session()->flash('error','User not updated at this time. Please try again');
        }

        return redirect()->route('user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->user =$this->user->find($id);
        if(!$this->user){
            request()->session()->flash('error','user not found');
            return redirect()->route('user.index');
        }
        $del =$this->user->delete();
        if($del){
            request()->session()->flash('success','user deleted successfully');
        }else{
            request()->session()->flash('error','problem while deleting User');
        }
        return redirect()->route('user.index');
    }
}
