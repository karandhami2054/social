<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Friend;
use App\Models\Message;
use App\Models\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserPanelController extends Controller
{
    protected $user= null;
    protected $post= null;
    protected $comment= null;
    protected $friend= null;
    protected $message= null;
    public function __construct(User $user, Post $post, Comment $comment, Friend $friend, Message $message){
        $this->user=$user;
        $this->post=$post;
        $this->comment=$comment;
        $this->friend=$friend;
        $this->message=$message;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->post=$this->post->orderBy('id','DESC')->get();
       
        
        $this->user=$this->user->where('id',Request()->user()->id)->get();
        return view('home.userpanel.index')->with('user_data',$this->user)->with('post_data',$this->post);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function user_post(Request $request)
    {
     //dd($request);   
     $rules =[
         'content'=>'required|string',
     ];
     $request->validate($rules);
     $data=$request->all();
     if($request->image){
        $file_name = uploadImage($request->image,'posts');
       // dd($file_name);
        if($file_name){
        $data['image'] = $file_name;
        }}
        

     //dd($data);
     $data['user_id']=Request()->user()->id;
     $data['date']=now();
     $data['topic_id']=1;
     $this->post=$this->post->fill($data);
     $this->post->save();
     return redirect()->route('userpanel.index');
    }

    public function user_update(Request $request,$id){
        $this->user=$this->user->find($id);
      //  dd($this->user);
        if(!$this->user){
            return redirect()->back()->with('alert','Username not found. Please try again later.');
        }
        return view('home.userpanel.update_form')->with('user_data',$this->user);
    }

    public function user_store(Request $request, $id){
        $request->request->add(['role'=>'user']);
       // dd($request);
        $rules =[
            'name'=>'required|string',
            'address'=>'required|string',
            'age'=>'required|integer',
            'contact'=>'required|integer',
            'status'=>'required|in:active,inactive',
            'gender'=>'required|in:male,female,other',
            'image'=>'sometimes|image|max:5000'
        ];
        $request->validate($rules);
        $data =$request->all();
        $this->user =$this->user->find($id);
      
            if($request->image){
            $file_name = uploadImage($request->image,'users');
           // dd($file_name);
            if($file_name){
            $data['image'] = $file_name;
           
            if(file_exists(public_path().'/images/users/'.$this->user->image))

            unlink(public_path().'/images/users/'.$this->user->image);
            }
            }
            $this->user=$this->user->fill($data);
        $success =$this->user->save();
        if(!$success){
            return redirect()->route('userpanel.index')->with('alert','Profile not  updated .Try again');      
        }
        return redirect()->route('userpanel.index')->with('alert','Profile updated successfully.');
    }

    public function my_post(Request $request){
        $user=$this->user->getUserDetails(Request()->user()->id);
        $this->post=$this->post->orderBy('id','DESC')->where('user_id',request()->user()->id)->paginate(10);
        $this->user=$this->user->where('id',Request()->user()->id)->get();
        return view('home.userpanel.my_posts')->with('user_data',$this->user)->with('post_data',$this->post)->with('user',$user);
    }

  public function members(){
    $this->friend=$this->friend->where('user1',Request()->user()->id)->orWhere('user2',Request()->user()->id)->get();
    $data=$this->user->where('role','user')->where('id','!=',Request()->user()->id)->where('status','active')->where('account','activate')->get();
    $this->user=$this->user->where('id',Request()->user()->id)->get();
    return view('home.userpanel.members')->with('user_data',$this->user)->with('data',$data)->with('friend',$this->friend);
  }



  public function search(Request $request){
      //dd($request);
    $data=$this->user->where('name','LIKE','%'. $request->search .'%' )->where('id','!=',$request->user()->id)->where('role','user')->where('status','active')->where('account','activate')->get();
    //dd($data);
    $this->user=$this->user->where('id',Request()->user()->id)->get();
   
    return view('home.userpanel.search')->with('user_data',$this->user)->with('data',$data);
  }

  public function user_details($id){
      //dd($id);
      $this->user=$this->user->where('id',$id)->get();
      $this->post=$this->post->orderBy('id','DESC')->where('user_id',$id)->get();
      return view('home.userpanel.user_details')->with('user_data',$this->user)->with('post_data',$this->post);
  }

  public function post_details($id){
      
    $this->user=$this->user->where('id',Request()->user()->id)->get(); 
    $this->post=$this->post->where('id',$id)->get();
    $this->comment=$this->comment->where('post_id',$id)->orderBy('id','ASC')->get();
// dd($this->comment);
    return view('home.userpanel.post_details')->with('user_data',$this->user)->with('post_data',$this->post)
                                              ->with('comment_data',$this->comment);
  }


  public function insert_comment(Request $request,$user_id, $id){
      //dd($request);
      $data= $request->all();
      //dd($data);
      $data['user_id']=$user_id;
      $data['post_id']=$id;
      $this->comment=$this->comment->fill($data);
      $this->comment->save();
      return redirect()->route('post_details',$id);
  }




  public function friends(){
      $this->friend=$this->friend->orderBy('id','DESC')->where('user1',Request()->user()->id)->orWhere('user2',Request()->user()->id)->get();
      $this->user=$this->user->where('id',Request()->user()->id)->get();
      return view('home.userpanel.friend')->with('user_data',$this->user)->with('friend_data',$this->friend);
    }


    public function addfriend($id){
        $data=null;
        $data['user1']= Auth::user()->id;
        $data['user2']= $id;
        $data['status1']= 'friend';
        $data['status2']= 'unfriend';
        $this->friend->fill($data);
        $this->friend->save();
       return redirect()->route('members');
    }


    public function cancel_request($id){
        $this->friend=$this->friend->find($id);
        $this->friend->delete();
       return redirect()->route('friends');
    }

    public function confirm_request($id){
        $data=null;
        $this->friend=$this->friend->find($id);
        $data['status1']='friend';
        $data['status2']='friend';
        $this->friend->fill($data);
        $this->friend->save();
       return redirect()->route('friends');
    }
  


    public function delete_comment($id){
        $this->comment=$this->comment->find($id);
        $this->comment->delete($id);
        return redirect()->route('post_details',$this->comment->post_id);
    }



    public function sendmessage(Request $request, $id){
       // dd($id);
    $message=$this->message->where('user1',$id)->orWhere('user2',$id)->get();
      $this->user=$this->user->where('id',Request()->user()->id)->get();
       return view('home.userpanel.message')->with('user_data',$this->user)->with('message',$message)->with('id',$id);
    }


    public function insert_message(Request $request,$id ){
        
        $data=$request->all();
        $data['user1']=Request()->user()->id;
        $data['user2']=$id;
        $this->message->fill($data);
        $this->message->save();
        return redirect()->route('sendmessage',$id);
    }


  




}
