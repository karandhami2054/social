<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    protected $post=null;
    public function __construct(Post $post)
    {
        $this->post=$post;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=$this->post->orderBy('id','DESC')->get();
        return view('admin.post.index')->with('postdata',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.post.form');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $rules=$this->post->getRules();
        $request->validate($rules);
      
        $data=$request->all();
        $data['date']=now();
        $this->post=$this->post->fill($data);
        $this->post->save();
        if($this->post){
            request()->session()->flash('success','Post added successfully');
        }
        else{
            request()->session()->flash('error','Post not added at this time. Please try again');
        }

        return redirect()->route('post.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->post=$this->post->find($id);
        if(!$this->post){
            request()->session()->flash('error','Post does not found');
            return redirect()->route('post.index');
        }
        return view('admin.post.form')->with('post_data',$this->post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules=$this->post->getRules();
        $request->validate($rules);
        //dd($request);
        $data=$request->all();
        $this->post= $this->post->find($id);
        $this->post=$this->post->fill($data);
        $this->post->save();

        if($this->post){
            request()->session()->flash('success','post updated successfully');
        }
        else{
            request()->session()->flash('error','post not updated at this time. Please try again');
        }

        return redirect()->route('post.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->post =$this->post->find($id);
        if(!$this->post){
            request()->session()->flash('error','post not found');
            return redirect()->route('post.index');
        }
        $del =$this->post->delete();
        if($del){
            request()->session()->flash('success','post deleted successfully');
        }else{
            request()->session()->flash('error','problem while deleting User');
        }
        return redirect()->route('post.index');
    }
}
