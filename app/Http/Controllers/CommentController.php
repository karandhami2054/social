<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    protected $comment=null;
    public function __construct(Comment $comment)
    {
        $this->comment=$comment;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=$this->comment->orderBy('id','DESC')->get();
        return view('admin.comment.index')->with('commentdata',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.comment.form');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules=$this->comment->getRules();
        $request->validate($rules);
      
        $data=$request->all();
        $data['date']=now();
        $this->comment=$this->comment->fill($data);
        $this->comment->save();
        if($this->comment){
            request()->session()->flash('success','Post added successfully');
        }
        else{
            request()->session()->flash('error','Post not added at this time. Please try again');
        }

        return redirect()->route('comment.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //dd($id);
        $this->comment=$this->comment->find($id);
        if(!$this->comment){
            request()->session()->flash('error','Comment does not found');
            return redirect()->route('comment.index');
        }
        return view('admin.comment.form')->with('comment_data',$this->comment);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules=$this->comment->getRules();
        $request->validate($rules);
        //dd($request);
        $data=$request->all();

      
        $this->comment= $this->comment->find($id);
        $this->comment=$this->comment->fill($data);
        $this->comment->save();

        if($this->comment){
            request()->session()->flash('success','User updated successfully');
        }
        else{
            request()->session()->flash('error','User not updated at this time. Please try again');
        }

        return redirect()->route('comment.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //dd($id);
        $this->comment =$this->comment->find($id);
      
        if(!$this->comment){
            request()->session()->flash('error','comment not found');
            return redirect()->route('comment.index');
        }
        $del =$this->comment->delete();
        if($del){
            request()->session()->flash('success','comment deleted successfully');
        }else{
            request()->session()->flash('error','problem while deleting User');
        }
        return redirect()->route('comment.index');
    }
}
