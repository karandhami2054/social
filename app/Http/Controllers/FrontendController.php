<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class FrontendController extends Controller
{
    protected $user=null;

    public function __construct(User $user){
        $this->user=$user;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('layouts.home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // dd($request);
       $rules= [
           'email'=>'required|email',
           'name'=>'required|string',
           'address'=>'required|string',
           'contact'=>'required|string',
           'gender'=>'required|in:male,female',
           'password'=>'required|string|confirmed',
       ];
       $data=$request->validate($rules);
      // dd($data);
      $data['password']=Hash::make($data['password']);
      $this->user=$this->user->fill($data);
      $success=$this->user->save();
      if($success){
        return redirect()->route('homepage')->with('alert','User added successfully. Please enter email and password to continue.');
    }else{
        return redirect()->route('homepage')->with('alert','User not added at this time');
    }

     
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function login1(Request $request){
        if (Auth::attempt(array('email' => $request->email, 'password' => $request->password))){
            
            if($request->user()->role =='user'){
                return redirect()->route('userpanel.index');
            }else{
                return redirect()->route('admin');
            }      
        }
    else{
        return redirect()->back()->with('alert','Username/password not match. Please try again later.');
    }

    }
}
