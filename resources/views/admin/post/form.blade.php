@extends('layouts.admin_dashboard')

@section('scripts')
<script>
    $('#change_password').change(function(){
        var checked =$('#change_password').prop('checked');
        if(checked){
            $('#password').attr('required');
            $('#password_confirm').attr('required');
            $('#change_password_div').removeClass('hidden');
        }else{
            $('#password').removeAttr('required');
            $('#password_confirm').removeAttr('required');
            $('#change_password_div').addClass('hidden');
        }
    });
</script>
@endsection

@section('content')

<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>User {{ isset($post_data) ? 'update' : 'add'}} </h3>

            </div>

            <div class="title_right">

            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2> User</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                    @if(isset($post_data))
                       {{Form::open(['url'=>route('post.update',$post_data->id),'class'=>'form', 'enctype'=>'multipart/form-data'])  }}
                            @method('PUT')


                        @else
                       {{Form::open(['url'=>route('post.store'),'class'=>'Form','enctype'=>'multipart/form-data']) }}
                    @endif
                       <div class="form-group row">
                            {{ Form::label('content', 'Content: ',['class'=>'col-sm-3']) }}
                            <div class="col-sm-8">
                                {{Form::text('content',@$post_data->content ,['class'=>'form-control','id'=>'content']) }}
                                @if($errors->has('content'))
                                <span class="alert-danger">{{ $errors->first('content') }}</span>
                                @endif
                            </div>
                        </div>

                      


                        <div class="form-group row" >
                            {{Form::label('topic_id','Topic Id:',['class'=>'col-sm-3']) }}
                            <div class="col-sm-8">
                                {{Form::number('topic_id',@$post_data->topic_id ,['class'=>'form-control'])}}
                                @if($errors->has('topic_id'))
                                <span class="alert-danger">{{$errors->first('topic_id')}}</span>
                                @endif
                            </div>
                        </div> 

                        <div class="form-group row">
                            {{ Form::label('user_id', 'User id: ',['class'=>'col-sm-3']) }}
                            <div class="col-sm-8">
                                {{Form::number('user_id',@$post_data->user_id ,['class'=>'form-control','id'=>'user_id']) }}
                                @if($errors->has('user_id'))
                                <span class="alert-danger">{{ $errors->first('user_id') }}</span>
                                @endif
                            </div>
                        </div>

                     

                        <div class="form-group row">
                            {{ Form::label('', ' ',['class'=>'col-sm-3']) }}
                            <div class="col-sm-8">
                                {{Form::button('submit',['class'=>'btn btn-success','id'=>'submit','type'=>'submit']) }}
                                {{Form::button('reset',['class'=>'btn btn-danger','id'=>'reset','type'=>'reset']) }}
                               
                            </div>
                        </div>



                       {{Form::close()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->

@endsection