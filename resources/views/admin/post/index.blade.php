@extends('layouts.admin_dashboard')

@section('content')
 <!-- page content -->
 <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
               <h3>Welcome to post page</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  
              <a href="{{ route('post.create')}}" class="btn btn-success pull-right"><i class="fas fa-plus"></i> Add post</a>
                  <div class="input-group">
                    
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>
            @include('admin.section.notification')

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>User Page</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table class="table">
                      <thead>
                        <th>S.n</th>
                        <th>post id</th>
                        <th>Post content</th>
                        <th>Post date</th>
                        <th>Topic id</th>
                        <th>User id</th>
                        
                        <th>Action</th>
                      </thead>
                      <tbody>
                       
                          @if($postdata)
                          @foreach($postdata as $key=>$data)
                          <tr>
                          <td>{{ $key+1}}</td>
                          <td>{{ $data->id}}</td>
                          <td>{{ $data->content}}</td>
                          <td>{{ $data->date}}</td>
                          <td>{{ $data->topic_id}}</td>
                          <td>{{ $data->user_id}}</td>
                        
                          <td>
                          <a href="{{ route('post.edit',$data->id)}}" style="border-radius: 50%" class="btn btn-success">
                                        <i class="fas fa-pencil-alt"></i></a>
  

                            <form action="{{ route('post.destroy',$data->id)}}" onsubmit="return confirm('Are you sure to delete')" method="post">
                                            @method('DELETE')
                                            @csrf
                                            <button class="btn btn-danger" style="border-radius: 50%">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        </form>
                          </tr>
                          @endforeach
                          @endif
                      
                      </tbody>

                    </table>
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

@endsection