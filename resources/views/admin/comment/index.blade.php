@extends('layouts.admin_dashboard')

@section('content')
 <!-- page content -->
 <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
               <h3>Welcome to comment page</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  
              <a href="{{ route('comment.create')}}" class="btn btn-success pull-right"><i class="fas fa-plus"></i> Add comment</a>
                  <div class="input-group">
                    
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>
            @include('admin.section.notification')

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>User Page</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table class="table">
                      <thead>
                        <th>S.n</th>
                        <th>comment id</th>
                        <th>Post id</th>
                        <th>user id</th>
                        <th>comment</th>
                        <th>Action</th>
                      </thead>
                      <tbody>
                       
                          @if($commentdata)
                          @foreach($commentdata as $key=>$data)
                          <tr>
                          <td>{{ $key+1}}</td>
                          <td>{{ $data->id}}</td>
                          <td>{{ $data->post_id}}</td>
                          <td>{{ $data->user_id}}</td>
                          <td>{{ $data->comment}}</td>    
                         <td>
                         <a href="{{ route('comment.edit',$data->id)}}" style="border-radius: 50%" class="btn btn-success">
                                        <i class="fas fa-pencil-alt"></i></a>
  

                            <form action="{{ route('comment.destroy',$data->id)}}" onsubmit="return confirm('Are you sure to delete')" method="post">
                                            @method('DELETE')
                                            @csrf
                                            <button class="btn btn-danger" style="border-radius: 50%">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        </form>
                         </td>
                            
                          </tr>                        
                          @endforeach
                          @endif
                      
                      </tbody>

                    </table>
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

@endsection