@extends('layouts.admin_dashboard')

@section('scripts')
<script>
    $('#change_password').change(function(){
        var checked =$('#change_password').prop('checked');
        if(checked){
            $('#password').attr('required');
            $('#password_confirm').attr('required');
            $('#change_password_div').removeClass('hidden');
        }else{
            $('#password').removeAttr('required');
            $('#password_confirm').removeAttr('required');
            $('#change_password_div').addClass('hidden');
        }
    });
</script>
@endsection

@section('content')

<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>User {{ isset($comment_data) ? 'update' : 'add'}} </h3>

            </div>

            <div class="title_right">

            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2> Comment</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                    @if(isset($comment_data))
                       {{Form::open(['url'=>route('comment.update',$comment_data->id),'class'=>'form', 'enctype'=>'multipart/form-data'])  }}
                            @method('PUT')


                        @else
                       {{Form::open(['url'=>route('comment.store'),'class'=>'Form','enctype'=>'multipart/form-data']) }}
                    @endif
                       <div class="form-group row">
                            {{ Form::label('comment', 'Comment: ',['class'=>'col-sm-3']) }}
                            <div class="col-sm-8">
                                {{Form::text('comment',@$comment_data->comment ,['class'=>'form-control','id'=>'comment']) }}
                                @if($errors->has('comment'))
                                <span class="alert-danger">{{ $errors->first('comment') }}</span>
                                @endif
                            </div>
                        </div>

                      


                        <div class="form-group row" >
                            {{Form::label('post_id','Post Id:',['class'=>'col-sm-3']) }}
                            <div class="col-sm-8">
                                {{Form::number('post_id',@$comment_data->post_id ,['class'=>'form-control'])}}
                                @if($errors->has('post_id'))
                                <span class="alert-danger">{{$errors->first('post_id')}}</span>
                                @endif
                            </div>
                        </div> 

                        <div class="form-group row">
                            {{ Form::label('user_id', 'User id: ',['class'=>'col-sm-3']) }}
                            <div class="col-sm-8">
                                {{Form::number('user_id',@$comment_data->user_id ,['class'=>'form-control','id'=>'user_id']) }}
                                @if($errors->has('user_id'))
                                <span class="alert-danger">{{ $errors->first('user_id') }}</span>
                                @endif
                            </div>
                        </div>

                     

                        <div class="form-group row">
                            {{ Form::label('', ' ',['class'=>'col-sm-3']) }}
                            <div class="col-sm-8">
                                {{Form::button('submit',['class'=>'btn btn-success','id'=>'submit','type'=>'submit']) }}
                                {{Form::button('reset',['class'=>'btn btn-danger','id'=>'reset','type'=>'reset']) }}
                               
                            </div>
                        </div>



                       {{Form::close()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->

@endsection