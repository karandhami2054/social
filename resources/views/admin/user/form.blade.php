@extends('layouts.admin_dashboard')

@section('scripts')
<script>
    $('#change_password').change(function(){
        var checked =$('#change_password').prop('checked');
        if(checked){
            $('#password').attr('required');
            $('#password_confirm').attr('required');
            $('#change_password_div').removeClass('hidden');
        }else{
            $('#password').removeAttr('required');
            $('#password_confirm').removeAttr('required');
            $('#change_password_div').addClass('hidden');
        }
    });
</script>
@endsection

@section('content')

<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>User {{ isset($user_data) ? 'update' : 'add'}} </h3>

            </div>

            <div class="title_right">

            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2> User</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                    @if(isset($user_data))
                       {{Form::open(['url'=>route('user.update',$user_data->id),'class'=>'form', 'enctype'=>'multipart/form-data'])  }}
                            @method('PUT')


                        @else
                       {{Form::open(['url'=>route('user.store'),'class'=>'Form','enctype'=>'multipart/form-data']) }}
                    @endif
                       <div class="form-group row">
                            {{ Form::label('name', 'Name: ',['class'=>'col-sm-3']) }}
                            <div class="col-sm-8">
                                {{Form::text('name',@$user_data->name ,['class'=>'form-control','id'=>'name']) }}
                                @if($errors->has('name'))
                                <span class="alert-danger">{{ $errors->first('name') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            {{Form::label('address','Address:',['class'=>'col-sm-3']) }}
                            <div class="col-sm-8">
                                {{Form::text('address',@$user_data->address ,['class'=>'form-control']) }}
                                @if($errors->has('name'))
                                <span class="alert-danger">{{$errors->first('name')}}</span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row" style="display: {{ isset($user_data) ? 'none' : 'block' }}">
                            {{Form::label('email','Email:',['class'=>'col-sm-3']) }}
                            <div class="col-sm-8">
                                {{Form::text('email',@$user_data->email ,['class'=>'form-control'])}}
                                @if($errors->has('email'))
                                <span class="alert-danger">{{$errors->first('email')}}</span>
                                @endif
                            </div>
                        </div> 

                        <div class="form-group row">
                            {{ Form::label('contact', 'Contact: ',['class'=>'col-sm-3']) }}
                            <div class="col-sm-8">
                                {{Form::tel('contact',@$user_data->contact ,['class'=>'form-control','id'=>'contact']) }}
                                @if($errors->has('contact'))
                                <span class="alert-danger">{{ $errors->first('contact') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            {{ Form::label('age', 'Age: ',['class'=>'col-sm-3']) }}
                            <div class="col-sm-8">
                                {{Form::number('age',@$user_data->age ,['class'=>'form-control','id'=>'age']) }}
                                @if($errors->has('age'))
                                <span class="alert-danger">{{ $errors->first('age') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            {{ Form::label('gender', 'Gender: ',['class'=>'col-sm-3']) }}
                            <div class="col-sm-8">
                                {{Form::select('gender',['male'=>'Male','female'=>'Female','other'=>'Other'] ,@$user_data->gender ,['class'=>'form-control','id'=>'gender']) }}
                                @if($errors->has('gender'))
                                <span class="alert-danger">{{ $errors->first('gender') }}</span>
                                @endif
                            </div>
                        </div>
                      
                        
                        <div class="form-group row">
                            {{ Form::label('role', 'Role: ',['class'=>'col-sm-3']) }}
                            <div class="col-sm-8">
                                {{Form::select('role',['admin'=>'Admin','user'=>'User'] ,'',['class'=>'form-control','id'=>'role']) }}
                                @if($errors->has('role'))
                                <span class="alert-danger">{{ $errors->first('role') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            {{ Form::label('status', 'Status: ',['class'=>'col-sm-3']) }}
                            <div class="col-sm-8">
                                {{Form::select('status',['active'=>'Active','inactive'=>'Inactive'] ,@$user_data->status ,['class'=>'form-control','id'=>'status']) }}
                                @if($errors->has('status'))
                                <span class="alert-danger">{{ $errors->first('status') }}</span>
                                @endif
                            </div>
                        </div>

                        
                        <div class="form-group row">
                            {{ Form::label('account', 'Account: ',['class'=>'col-sm-3']) }}
                            <div class="col-sm-8">
                                {{Form::select('account',['activate'=>'Activate','deactivate'=>'deactivate'] ,@$user_data->account ,['class'=>'form-control','id'=>'account']) }}
                                @if($errors->has('account'))
                                <span class="alert-danger">{{ $errors->first('account') }}</span>
                                @endif
                            </div>
                        </div>
                        @if(isset($user_data))

                        <div class="form-group row" style=" isset($user_data) ? 'none' : 'block'  ">
                            {{ Form::label('change_password', 'Change password: ',['class'=>'col-sm-3']) }}
                            <div class="col-sm-9">
                                {{Form::checkbox('change_password',1,false,['id'=>'change_password']) }} Yes
                                @if($errors->has('change_password'))
                                <span class="alert-danger">{{ $errors->first('change_password') }}</span>
                                @endif
                            </div>
                        </div>
                        @endif


                        <div class=" {{ isset($user_data)?'hidden':''}}" id="change_password_div">

                        <div class="form-group row" style=" isset($user_data) ? 'none' : 'block'  ">
                            {{ Form::label('password', 'Password: ',['class'=>'col-sm-3']) }}
                            <div class="col-sm-9">
                                {{Form::password('password',['id'=>'password','class'=>'form-control']) }}
                                @if($errors->has('password'))
                                <span class="alert-danger">{{ $errors->first('password') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row" style=" isset($user_data) ? 'none' : 'block'  ">
                            {{ Form::label('confirm_password', 'Reenter password: ',['class'=>'col-sm-3']) }}
                            <div class="col-sm-9">
                                {{Form::password('password_confirmation',['id'=>'confirm_password','class'=>'form-control']) }}
                                @if($errors->has('confirm_password'))
                                <span class="alert-danger">{{ $errors->first('confirm_password') }}</span>
                                @endif
                            </div>
                        </div>

                        </div>

                        <div class="form-group row">
                            {{ Form::label('', ' ',['class'=>'col-sm-3']) }}
                            <div class="col-sm-8">
                                {{Form::button('submit',['class'=>'btn btn-success','id'=>'submit','type'=>'submit']) }}
                                {{Form::button('reset',['class'=>'btn btn-danger','id'=>'reset','type'=>'reset']) }}
                               
                            </div>
                        </div>



                       {{Form::close()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->

@endsection