<!DOCTYPE html>
<html>
<head>
	<title>Welcome user!</title>
	<link rel="stylesheet" type="text/css" href="{{asset('assets/index/styles/home.styles.css')}}" media="all" >
</head>
<body>
	<!-- container starts  -->
	<div class="container">
		<!-- header wrapper starts  -->
		  <div id="head_wrap">
			<!--  header starts  -->
			  <div id="header">
			  	<ul id="menu">
			  		<li><a href="{{route('userpanel.index')}}">Home</a></li>
			  		<li><a href="members.php">members</a></li>
			  		<strong>Topics:</strong>
			  		
			  	</ul>
			  	<form method="get" action="results.php" id="form1">
			  		<input type="text" name="user_query" placeholder="search a topic">
			  		<input type="submit" name="search" value="search">
			  		
			  	</form>

				
			  </div>
			<!-- header ends  -->
			
		  </div>
		<!--  header wrapper ends  -->

		<!--  content area starts -->
		   <div class="content">
			<!-- user timeline starts  -->
			   <div id="user_timeline">
			   	<div id="user_details">
               
                        <img src='user/user_images/$user_image' width='200' height='200'/>
                        </center>
                        <div id='user_mention'>
                        <p><strong> Name: </strong>{{ $user_data[0]['name']}}</p>
                        <p><strong> Email: </strong> {{ $user_data[0]['email']}}</p>
                        <p><strong> Address:</strong> {{ $user_data[0]['address']}}</p>
                        <p><strong> Age:</strong> {{ $user_data[0]['age']}}</p>
                        <p> <a > Messages</a></p>
                        <p><a>My posts </a></p>
                        <p><a>Edit my account</a></p>
                        <p>

						<a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                         <i class="fa fa-sign-out pull-right"></i> Log Out

                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                             </form>
						</p>
                        </div>
                 		   		
			   	</div>
				 
                 
			   </div>
			   <!-- user timeline ends here  -->
               @yield('content')
           </div>
         
		<!-- content area ends  -->

	</div>
	<!--  container ends  -->

</body>
</html> 