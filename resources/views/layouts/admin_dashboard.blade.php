@include('admin.section.header')

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>Admin CMS!</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            @include('admin.section.menu')
            <!-- /menu profile quick info -->

            <br />

         @include('admin.section.sidebar')

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        @include('admin.section.top_nav')
        <!-- /top navigation -->

      @yield('content')
        <!-- footer content -->
        @include('admin.section.footer')
        <!-- /footer content -->
      </div>
    </div>

    @include('admin.section.scripts')
  
  </body>
</html>
