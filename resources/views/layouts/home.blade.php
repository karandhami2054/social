
<html>
<head>
	<title> Facebook </title>

	<LINK REL="SHORTCUT ICON" HREF="{{ asset('assets/home/fb_files/fb_title_icon/Faceback.ico')}}"/>
	<link href="{{asset('assets/home/fb_files/fb_index_file/fb_css_file/index_css.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/home/fb_files/fb_font/font.css')}}" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="{{asset('assets/home/fb_files/fb_index_file/fb_js_file/Registration_validation.js')}}"> </script>
	<script>
		var msg = '{{Session::get('alert')}}';
		var exist = '{{Session::has('alert')}}';
		if (exist) {
			alert(msg);
		}
	</script>
</head>
<script>
	function time_get()
	{
		d = new Date();
		mon = d.getMonth()+1;
		time = d.getDate()+"-"+mon+"-"+d.getFullYear()+" "+d.getHours()+":"+d.getMinutes();
		Reg.fb_join_time.value=time;
	}
</script>
<body>
	<!--login form-->
	<form method="post" action="{{ route('login1') }}">
		@csrf
		<div style="position:absolute; left:57.7%; top:2.2%; font-size:12px; color:#FFFFFF;">   Email </div> 
		<div style="position:absolute; left:57.7%; top:5.18%; font-size:11px; "> <input type="text" name="email" style="width:149.5;"/> </div>
		<div style="position:absolute; left:57.4%; top:8.8%; font-size:12; color:#CCCCCC;">  <input type="checkbox" checked="checked">   Keep me logged in </div>
		<div style="position:absolute;left:69.6%; top:2.2%; font-size:13px; color:#FFFFFF"> Password </div>
		<div style="position:absolute;left:69.6%; top:5.18%; font-size:13px; "> <input type="password" name="password" style="width:149.5;"> </div>
		<div style="position:absolute;left:69.6%; top:9.2%; font-size:12px; color:#CCCCCC;"> <a href="Forgot_Password.php" style="color:#CCCCCC; text-decoration:none;"> Forgot your password? </a> </div>   
		<div style="position:absolute;left:81.8%;top:5.2%; ">   <button class="btn alert_danger" type="submit">submit</button>  </div>
	</form>
	
	<!-- Faceback left part -->
	
		<!--Left part-->
		<!--Mobile Image--> 	
	<div style="position:absolute; left:5%; top:35%;"> <img src="{{asset('assets/home/fb_files/fb_index_file/fb_image_file/Faceback_map.PNG')}}" width="700" height="275"> </div>
    <div style="position:absolute; left:7%; top:24%; color:#3B5998; font-size:28px;"> <font face="myFbFont"> Facebook helps you connect and share with </font> </div>
    <div style="position:absolute; left:7%; top:30%; color:#3B5998; font-size:28px;"> <font face="myFbFont"> the people in your life. </font></div>
	
	
	
	<!-- Registration -->
	<form  method="post" onSubmit="return check();" action="{{ route('frontend.store') }}">
        @csrf
		<div style="position:absolute;left:58%; top:14.5%; color:#000066; font-size:25"> <h5> Sign Up </h5> </div>
		<div style="position:absolute;left:58%; top:24.6%; color:#000000;">  It's free and always will be.  </div>
		<div style="position:absolute;left:57.3%; top:29.1%; height:1; width:385; background-color:#CCCCCC;"> </div>
        
		<div style="position:absolute;left:59.4%; top:34%; font-size:16px; color:#000000">Name: </div>
        <div style="position:absolute;left:65.2%;   top:32.8%; "> <input type="text" name="name" class="inputbox" maxlength="10"/> 
        @if($errors->has('name'))
                                <span class="alert-danger">{{ $errors->first('name') }}</span>
                                @endif
    </div>
		<div style="position:absolute;left:59.4%; top:41%; font-size:16px; color:#000000">  Address: </div>
        <div style="position:absolute;left:65.2%;  top:39.8%;  "> <input type="text" name="address"  size="25" class="inputbox" maxlength="100" /> 
        @if($errors->has('address'))
                                <span class="alert-danger">{{ $errors->first('address') }}</span>
                                @endif</div>
        
        
        <div style="position:absolute;left:59.2%; top:48%; font-size:16px; color:#000000">  Your Email:  </div>
        <div style="position:absolute;left:65.2%;  top:46.8%; "> <input type="text" name="email"  size="25" class="inputbox" /> 
        @if($errors->has('email'))
                                <span class="alert-danger">{{ $errors->first('email') }}</span>
                                @endif</div>


        <div style="position:absolute;left:57.4%; top:55%; font-size:16px; color:#000000"> New Password:  </div>
        <div style="position:absolute;left:65.2%; top:54%; "> <input type="password" name="password" size="25" class="inputbox" /> 
        @if($errors->has('password'))
                                <span class="alert-danger">{{ $errors->first('password') }}</span>
                                @endif</div>
   
        
        
        
        <div style="position:absolute;left:57.4%; top:62%; font-size:16px; color:#000000"> Confirm Password:  </div>
        <div style="position:absolute;left:65.2%; top:60.8%; "> <input type="password" name="password_confirmation" size="25" class="inputbox" /> 
        @if($errors->has('password_confirmation'))
                                <span class="alert-danger">{{ $errors->first('password_confirmation') }}</span>
                                @endif</div>
    
		<div style="position:absolute;left:62.2%; top:68.5%; font-size:16px; color:#000000"> I am:  </div>
		<div style="position:absolute;left:65.2% ;top:67.8%;">		  
		<select name="gender" style="width:120;height:35;font-size:18px;padding:3;">
			<option value="Select Sex:" required> Select Sex: </option>
			<option value="female"> Female </option>
            <option value="male"> Male </option>
            @if($errors->has('gender'))
                                <span class="alert-danger">{{ $errors->first('gender') }}</span>
                                @endif
        </select>
        
		</div>
		
<div style="position:absolute;left:60.28%; top:74.8%; font-size:16px; color:#000000">  contact:  </div>

		<div style="position:absolute;left:65.2%; top:74.8%; "> <input type="tel" name="contact" size="25" class="inputbox" /> </div>
		

	




	
		<div style="position:absolute;left:65.2%; top:82%; ">  <input type="submit" > </div>
		</form>
		
		<div style="position:absolute;left:57.3%; top:90%; height:1; width:385; background-color:#CCCCCC; "> </div> 
        
 <!--my_details -->  
    <div style="display:none;" id="my_details">
    <div style="position:absolute;left:12%;top:73%; height:30%; width:30%; z-index:2; background:#000; opacity:0.5; box-shadow:10px 0px 10px 1px rgb(0,0,0);">   </div>
    <div style="position:absolute;left:13%;top:75%; z-index:3;"> <img src="{{asset('assets/home/fb_files/fb_index_file/fb_background_file/Developer_details/my.jpg')}}" height="165" width="150" style="box-shadow:0px 0px 10px 5px rgb(0,0,0);"> </div>
    <div style="position:absolute;left:26%;top:75%; z-index:3; color:#FFF;"> <h2> <?php echo base64_decode("QW1pdCBEb2RpeWEgKEFEKQ=="); ?> </h2> </div>
    <div style="position:absolute;left:26%;top:83%; z-index:3; color:#FFF;">  <h3><?php echo base64_decode("QW1pdC5hZDFpNEB5YWhvby5jb20="); ?> </h3> </div>
    <div style="position:absolute;left:26%;top:90%; z-index:3; color:#FFF;"> <h3> <?php echo base64_decode("NzYwMDg5ODIxMA=="); ?>  </h3> </div>
	</div>
    
		
					
</body>
</html>