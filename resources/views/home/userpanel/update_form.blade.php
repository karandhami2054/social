<!DOCTYPE html>
<html>
<head>
	
	<link rel="stylesheet" type="text/css" href="{{asset('assets/index/styles/home.styles.css')}}" media="all" >
    <title>Form page</title>
		<link rel="stylesheet" href="{{asset('assets/admin/css/bootstrap.min.css')}}">
		
</head>
<body>
	<!-- container starts  -->
	<div class="container">
		<!-- header wrapper starts  -->
		  <div id="head_wrap">
			<!--  header starts  -->
			  <div id="header">
			  	<ul id="menu">
			  		<li><a href="{{route('userpanel.index')}}">Home</a></li>
			  		<li><a href="members.php">members</a></li>
			  		<strong>Topics:</strong>
			  		
			  	</ul>
			  	<form method="get" action="{{route('search')}}" id="form1">
			  		<input type="text" name="search" placeholder="search a topic">
			  		<input type="submit" value="search">
			  		
			  	</form>

				
			  </div>
            <!-- header ends  -->
            <br><br>
			
          </div>
          </div>
		<!--  header wrapper ends  -->

		<!--  content area starts -->
		  
            <!-- user timeline starts  -->
        <div class="container" style="background:white">
        <h3 style="text-align:center">Your Details are:</h3>
			<div class="row">
				<div class="col-md-12">
					<form action="{{route('user_store',$user_data->id)}}" method="post" class="form" enctype="multipart/form-data">
					@csrf
						<div class="form-group row">
							<label for="" class="col-sm-3">Full Name:</label>
							<div class="col-sm-9">
								<input type="text" name="name" class="form-control" id="name" required value="{{@$user_data->name}}">
								@if($errors->has('name'))
								<p class="alert-danger">{{$errors->first('name')}}</p>
								@endif
							</div>
						</div>

						<div class="form-group row">
							<label for="" class="col-sm-3">Address:</label>
							<div class="col-sm-9">
								<input type="text" name="address" class="form-control" required value="{{@$user_data->address}}">
								@if($errors->has('address'))
								<p class="alert-danger">{{$errors->first('address')}}</p>
								@endif
							</div>
						</div>
					

						<div class="form-group row">
							<label for="" class="col-sm-3">Age:</label>
							<div class="col-sm-9">
								<input type="number" name="age" class="form-control" id="age" required value="{{@$user_data->age}}">
								@if($errors->has('age'))
								<p class="alert-danger">{{$errors->first('age')}}</p>
								@endif
							</div>
                        </div>
                        
                        <div class="form-group row">
							<label for="" class="col-sm-3">Contact:</label>
							<div class="col-sm-9">
								<input type="tel" name="contact" class="form-control" id="contact" required value="{{@$user_data->contact}}">
								@if($errors->has('contact'))
								<p class="alert-danger">{{$errors->first('contact')}}</p>
								@endif
							</div>
						</div>

						
						
                        
                        <div class="form-group row">
							<label for="" class="col-sm-3">Status:</label>
							<div class="col-sm-9">
								<select name="status" id="status" class="form-control">
									<option value="active"{{ ($user_data->status =='active') ? 'selected' : '' }}>Active</option>
									<option value="inactive" {{ ($user_data->status =='inactive') ? 'selected' : '' }}>Inactive</option>
								</select>
								@if($errors->has('status'))
								<p class="alert-danger">{{$errors->first('status')}}</p>
								@endif
							</div>
						</div>	
						
						<div class="form-group row">
							<label for="" class="col-sm-3">Gender:</label>
							<div class="col-sm-9">
								<select name="gender" id="gender" class="form-control">
									<option value="male"{{ ($user_data->gender =='male') ? 'selected' : '' }}>Male</option>
									<option value="female" {{ ($user_data->gender =='female') ? 'selected' : '' }}>Female</option>
									<option value="other" {{ ($user_data->gender =='other') ? 'selected' : '' }}>Other</option>
								</select>
								@if($errors->has('gender'))
								<p class="alert-danger">{{$errors->first('gender')}}</p>
								@endif
							</div>
						</div>

						<div class="form-group row">
                            {{ Form::label('image', 'Image: ',['class'=>'col-sm-3']) }}
                            <div class="col-sm-4">
                                {{Form::file('image',['class'=>'form-control','id'=>'image','accept'=>'image/*']) }}
                                @if($errors->has('image'))
                                <span class="alert-danger">{{ $errors->first('image') }}</span>
                                @endif
                            </div>
                            @if(isset($user_data) && file_exists(public_path().'/images/users/'.$user_data->image))
                            <div class="col-sm-4">
                                <img src="{{ asset('images/users/'.$user_data->image )}} " class="img img-responsive img-thumbnail">
                            </div>
                            @endif
                        </div>


						<div class="form-group row">
							<label for="" class="col-sm-3"></label>
							<div class="col-sm-9">
								<button class="btn btn-success" type="submit">
									Submit
								</button>
							</div>
						</div>
						
						
												
					</form>
				</div>
			</div>
		</div>
	
			
			   <!-- user timeline ends here  -->
               

        
		<!-- content area ends  -->

	</div>
	<!--  container ends  -->

</body>
</html> 