<!DOCTYPE html>
<html>

<head>
	<title>Welcome user!</title>
	<link rel="stylesheet" type="text/css" href="{{asset('assets/index/styles/home.styles.css')}}" media="all">

</head>

<body>
	<!-- container starts  -->
	<div class="container">
		<!-- header wrapper starts  -->
		<div id="head_wrap">
			<!--  header starts  -->
			<div id="header">
				<ul id="menu">
					<li><a href="{{route('userpanel.index')}}">Home</a></li>
					<li><a href="{{route('members')}}">Members</a></li>
					<li><a href="{{route('friends')}}">My friends</a></li>
				</ul>
				<form method="get" action="{{route('search')}}" id="form1">
					<input type="text" name="search" placeholder="search a user">
					<input type="submit" value="search">

				</form>


			</div>
			<!-- header ends  -->

		</div>
		<!--  header wrapper ends  -->

		<!--  content area starts -->
		<div class="content">
			<!-- user timeline starts  -->
			<div id="user_timeline">
				<div id="user_details">

					<img src="{{asset('images/users/'.$user_data[0]['image'])}}" width='200' height='200' />
					</center>
					<div id='user_mention'>
						<p><strong> Name: </strong>{{ $user_data[0]['name']}}</p>
						<p><strong> Email: </strong> {{ $user_data[0]['email']}}</p>
						<p><strong> Address:</strong> {{ $user_data[0]['address']}}</p>
						<p><strong> Age:</strong> {{ $user_data[0]['age']}}</p>
						<p> <a> Messages</a></p>
						<p><a href="{{ route('my_post') }}">My posts </a></p>
						<p><a href="{{route('user_update',$user_data[0]['id'])}}">Edit my account</a></p>
						<p><a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
								<i class="fa fa-sign-out pull-right"></i> Log Out

							</a>

							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
								@csrf
							</form>


						</p>
					</div>

				</div>


			</div>
			<!-- user timeline ends here  -->

			<!--  content timeline starts  -->
			<div id="content_timeline">
				@foreach($data as $key=>$value)
				<div id='posts'>
					<p> <img src="{{asset('images/users/'.$value->image)}}" width='200' height='auto'> </p>
					<a href="{{ route('user_details',$value->id)}}" style="text-decoration:none">
						<p> {{$value->name}} </p>
						<p> {{$value->address}} </p>
					</a>	
					@foreach($friend as $k=>$v)		
				
						@if($value->id == $v->user2)
							@if($v->status2 == 'unfriend')
								<a href="{{route('cancel_request',$v->id)}}" id="cancel" style="display:block">Cancel Request</a>
							@elseif($v->status1 == 'friend')
								@if($v->status2 == 'friend')
									<h5>Friend</h5>
								@else 
								<a href="{{route('addfriend',$value->id)}}" id="add">Add friend Request</a>
								@endif		
							@else		
							@endif
							@else
							@endif
						@if($value->id == $v->user1)
							@if($v->status1 == 'friend')
								@if($v->status2 == 'friend')
									<h5 id="friend">Friend</h5>
								@else 
								@endif
							@else
							@endif							
						@endif

					
						
					@endforeach
					
				</div><br><br>
				@endforeach

			</div>
			<!-- content timeline ends  -->

		</div>

		<!-- content area ends  -->

	</div>
	<!--  container ends  -->

</body>

</html>