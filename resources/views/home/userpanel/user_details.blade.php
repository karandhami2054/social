<!DOCTYPE html>
<html>
<head>
	<title>Welcome user!</title>
	<link rel="stylesheet" type="text/css" href="{{asset('assets/index/styles/home.styles.css')}}" media="all" >
	
</head>
<body>
	<!-- container starts  -->
	<div class="container">
		<!-- header wrapper starts  -->
		  <div id="head_wrap">
			<!--  header starts  -->
			  <div id="header">
			  	<ul id="menu">
			  		<li><a href="{{route('userpanel.index')}}">Home</a></li>
			  		<li><a href="{{ route('members')}}">members</a></li>
					  <li><a href="{{route('friends')}}">My friends</a></li>
			  		
			  	</ul>
			  	<form method="get" action="{{route('search')}}" id="form1">
			  		<input type="text" name="search" placeholder="search a user">
			  		<input type="submit" name="search" value="search">
			  		
			  	</form>

				
			  </div>
			<!-- header ends  -->
			
		  </div>
		<!--  header wrapper ends  -->

		<!--  content area starts -->
		   <div class="content">
			<!-- user timeline starts  -->
			   <div id="user_timeline">
			   	<div id="user_details">
               
                        <img src="{{asset('images/users/'.$user_data[0]['image'])}}" width='200' height='200'/>
                        </center>
                        <div id='user_mention'>
                        
                        <p><strong> Name: </strong>{{ $user_data[0]['name']}}</p>
                        <p><strong> Email: </strong> {{ $user_data[0]['email']}}</p>
                        <p><strong> Address:</strong> {{ $user_data[0]['address']}}</p>
                        <p><strong> Age:</strong> {{ $user_data[0]['age']}}</p>
                        <p><strong> Contact:</strong> {{ $user_data[0]['contact']}}</p>
                        <p><strong> Gender:</strong> {{ $user_data[0]['gender']}}</p>
                       
                       
                        </div>
                 		   		
			   	</div>
				 
                 
			   </div>
			   <!-- user timeline ends here  -->
              
<!--  content timeline starts  -->
<div id="content_timeline">
@if($post_data->count() > 0)
	@foreach($post_data as $key=>$value)
	<div id='posts'>
		<p> <img src="{{asset('images/posts/'.$value->image)}}" width='200' height='auto'> </p>
		<h3> <a href="{{ route('post_details',$value->id)}}">{{username($value->user_id)}}</a> </h3>

		<p> {{$value->date}} </p>
		<p> {{$value->content}} </p>
		<a href="{{ route('post_details',$value->id)}}" style='float:right; ' ><button> see Replies or Reply to this post </button></a>
	</div><br /> <br>
    @endforeach
    @else
    <div id='posts'>
	No posts found.
		</div><br /> <br>


    @endif
	<!-- {{dd($post_data)}} -->
</div>
<!-- content timeline ends  -->

           </div>
         
		<!-- content area ends  -->

	</div>
	<!--  container ends  -->

</body>
</html> 