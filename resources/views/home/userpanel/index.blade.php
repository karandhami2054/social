<!DOCTYPE html>
<html>
<head>
	<title>Welcome user!</title>
	<link rel="stylesheet" type="text/css" href="{{asset('assets/index/styles/home.styles.css')}}" media="all" >
	
</head>
<body>
	<!-- container starts  -->
	<div class="container">
		<!-- header wrapper starts  -->
		  <div id="head_wrap">
			<!--  header starts  -->
			  <div id="header">
			  	<ul id="menu">
			  		<li><a href="{{route('userpanel.index')}}">Home</a></li>
					  <li><a href="{{route('members')}}">All Users</a></li>
					  <li><a href="{{route('friends')}}">My friends</a></li>
					  
			  		
			  		
			  	</ul>
			  	<form method="get" action="{{route('search')}}" id="form1">
			  		<input type="text" name="search" placeholder="search a topic">
			  		<input type="submit" value="search">
			  		
			  	</form>

				
			  </div>
			<!-- header ends  -->
			
		  </div>
		<!--  header wrapper ends  -->

		<!--  content area starts -->
		   <div class="content">
			<!-- user timeline starts  -->
			   <div id="user_timeline">
			   	<div id="user_details">
               
                        <img src="{{asset('images/users/'.$user_data[0]['image'])}}" width='200' height='200'/>
                        </center>
                        <div id='user_mention'>
                        <p><strong> Name: </strong>{{ $user_data[0]['name']}}</p>
                        <p><strong> Email: </strong> {{ $user_data[0]['email']}}</p>
                        <p><strong> Address:</strong> {{ $user_data[0]['address']}}</p>
                        <p><strong> Age:</strong> {{ $user_data[0]['age']}}</p>
                        <p> <a href="{{ route('friends')}}"> Messages</a></p>
                        <p><a  href="{{ route('my_post') }}">My posts </a></p>
                        <p><a href="{{route('user_update',$user_data[0]['id'])}}">Edit my account</a></p>
						<p><a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                         <i class="fa fa-sign-out pull-right"></i> Log Out

                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                             </form>

						
						</p>
                        </div>
                 		   		
			   	</div>
				 
                 
			   </div>
			   <!-- user timeline ends here  -->
              
<!--  content timeline starts  -->
<div id="content_timeline">
	<form action="{{route('user_post')}}" method="post" id="f" enctype="multipart/form-data">
		@csrf
		<h2>How do you feel today.</h2>
	
		<textarea cols="83" rows="4" name="content" placeholder="write description.." style="resize:none"></textarea>
		<input type="file" name="image"  required="" accept="image/*">

		<input type="submit" value="post to timeline" />
	</form>

	<h3>Most Recent Discussion</h3><br><br>
	@foreach($post_data as $key=>$value)
	<div id='posts'>
		<p> <img src="{{asset('images/posts/'.$value->image)}}" width='200' height='auto'> </p>
		<h3> <a href="{{ route('user_details',$value->user_id)}}"> {{ username($value->user_id) }}</a> </h3>

		<h6> {{$value->date}} </h6>
		<h5> {{$value->content}} </h5>
		<a href="{{ route('post_details',$value->id)}}" style='float:right; ' ><button> see Replies or Reply to this post </button></a>
	</div><br /> <br>
	@endforeach
	<!-- {{dd($post_data)}} -->
</div>
<!-- content timeline ends  -->

           </div>
         
		<!-- content area ends  -->

	</div>
	<!--  container ends  -->

</body>
</html> 