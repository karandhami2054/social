<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFriendsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('friends', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user1');
            $table->unsignedBigInteger('user2');
            $table->enum('status1',['friend','unfriend'])->default('unfriend');
            $table->enum('status2',['friend','unfriend'])->default('unfriend');
            $table->foreign('user1')->references('id')->on('users')->onDelete('CASCADE');
            $table->foreign('user2')->references('id')->on('users')->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('friends');
    }
}
