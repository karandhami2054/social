<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array =array(
            array(
                'name'=>'karan',
                'email'=>'admin@fb.com',
                'role'=>'admin',
                'password'=>Hash::make('admin123'),
                'status'=>'active',
                'account'=>'activate',
                'contact'=>'99414022'
            ),
            array(
                'name'=>'other',
                'email'=>'user@fb.com',
                'role'=>'user',
                'password'=>Hash::make('user123'),
                'status'=>'active',
                'account'=>'activate',
                'contact'=>'99414022'
            )
        );
        DB::table('users')->insert($array);
    }
}
