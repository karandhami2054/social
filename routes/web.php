<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix'=>'admin'], function(){
    Route::get('/', 'HomeController@admin')->name('admin');
    Route::resource('/user', 'UserController');
    Route::resource('/post', 'PostController');
    Route::resource('/comment', 'CommentController');
});



Route::get('/user', 'HomeController@user')->name('user');


Route::resource('/frontend', 'FrontendController');
Route::get('/', 'FrontendController@index')->name('homepage');
Route::post('/login1', 'FrontendController@login1')->name('login1');


Route::group(['prefix'=>'user'], function(){
    Route::resource('/userpanel','UserPanelController');
    Route::post('/user_post','UserPanelController@user_post')->name('user_post');
    Route::get('/user_update/{id}','UserPanelController@user_update')->name('user_update');
    Route::post('/user_store/{id}','UserPanelController@user_store')->name('user_store');
    Route::get('/my_post','UserPanelController@my_post')->name('my_post');
    Route::get('/members','UserPanelController@members')->name('members');
    Route::get('/search','UserPanelController@search')->name('search');
    Route::get('/user_details/{id}','UserPanelController@user_details')->name('user_details');
    Route::get('/post_details/{id}','UserPanelController@post_details')->name('post_details');
    Route::post('/insert_comment/{user_id}/{id}','UserPanelController@insert_comment')->name('insert_comment');

    Route::get('/delete_comment/{id}','UserPanelController@delete_comment')->name('delete_comment');

    Route::get('/friends','UserPanelController@friends')->name('friends');

    Route::get('/addfriend/{id}','UserPanelController@addfriend')->name('addfriend');


    Route::get('/cancel_request/{id}','UserPanelController@cancel_request')->name('cancel_request');
    Route::get('/confirm_request/{id}','UserPanelController@confirm_request')->name('confirm_request');

    Route::get('/sendmessage/{id}','UserPanelController@sendmessage')->name('sendmessage');

    Route::post('/insert_message/{id}','UserPanelController@insert_message')->name('insert_message');


});
